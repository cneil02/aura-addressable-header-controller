#define CORSAIR_LIGHTING_SDK_DISABLE_DEPRECATION_WARNINGS
#define MAX_STR 255

#include "aura_hid.hpp"
#include <stdio.h>
#include <iostream>

int main() {
	AuraHIDDevices aura_devs;
	int n = detectAuraHIDDevice(aura_devs);
	if (n < 1) {
		printf("No AURA ARGB Devices Found\n");
		return 1;
	}
	else {
		printf("Found %d %s\n", n, (n == 1 ? "device" : "devices"));
	}

	int res;
	uint8_t buff[AURA_ARGB_MSG_LEN];

	buff[0] = MSG_START;
	for (int i = 1; i < AURA_ARGB_MSG_LEN; i++) {
		buff[i] = 0x00;
	}

	wchar_t wstr[MAX_STR];
	hid_device* handle;

	printf("\nOpening Controller 1...\n");
	handle = hid_open(aura_devs[0].vendor_id, aura_devs[0].product_id, nullptr);
	if (!handle) {
		printf("Unable to open device\n");
		return 1;
	}

	printf("Device Opened\n");

	res = hid_get_manufacturer_string(handle, wstr, MAX_STR);
	wprintf(L"\t%ls\n", wstr);

	//Direct-Mode-Example--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	buff[1] = AURA_EFFECT_MODE;
	buff[4] = 0xff;
	hid_write(handle, buff, AURA_ARGB_MSG_LEN); //This message must be sent to make direct mode work after the device has been in effect mode.


	buff[1] = AURA_DIRECT_MODE; //Control Mode. AURA_EFFECT_MODE for presets; AURA_DIRECT_MODE for precise LED control.
	buff[2] = 0x00; //Device
	buff[3] = 0x00; //Start LED. You can control up to 20 LEDs in each message. If you need more, send multiple messages with different start LEDs.
	buff[4] = 0x14; //In Direct mode, set to the number of LEDs you want to control. Can be set higher than the number of LEDs you have without causing issues.

	//buff[5] to buff[64] are the main body of the message. It is in the form R0, G0, B0, R1, G1, B1, ... where Rn is the intensity of the red channel on the nth LED from the start LED. (e.g. start 0xff, 0x00, 0x00, 0x00, 0xff, 0x00, 0x00, 0x00, 0xff to set the first 3 LEDs to Red, Green and Blue respectively).

	for (int i = 5; i < AURA_ARGB_MSG_LEN; i += 4) { //Creates message to set all LED colours in pattern RGBRGBRGB...
		buff[i] = 0xff;
	}

	hid_write(handle, buff, AURA_ARGB_MSG_LEN); //Sends message.

	buff[2] = 0x80; //I'm not sure why but in direct mode, you have to send a second message on device 0x80 to make it work.
	hid_write(handle, buff, AURA_ARGB_MSG_LEN); //Sends second message.

	//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	/*//Effect-Mode-Example--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	buff[1] = AURA_EFFECT_MODE;
	buff[2] = 0x00;
	buff[3] = 0x00;
	buff[4] = AURA_MODE_BREATHING; //In Effect mode, set to one of the presets listed in aura_hid.hpp.

	//Only the first 3 bytes of the message body matter in effect mode. They determine the colour for the whole device. Individual LEDs cannot be controlled. In rainbow or spectrum modes the main body doesn't matter as all colours are controlled by the effect.

	buff[5] = 0xff; //Red Channel
	buff[6] = 0x45; //Green Channel
	buff[7] = 0x00; //Blue Channel

	hid_write(handle, buff, AURA_ARGB_MSG_LEN);

	*///---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- -

	hid_close(handle);

	return 0;
}